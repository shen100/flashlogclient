package com.shen100
{
	import com.shen100.vo.LogVO;
	
	import flash.errors.IOError;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	import flash.events.StatusEvent;
	import flash.net.LocalConnection;
	import flash.net.Socket;
	import flash.utils.getQualifiedClassName;
	
	public class FlashLog
	{
		private static const DEBUG:int 	= 1;
		private static const INFO:int 	= 2;
		private static const WARN:int 	= 3;
		private static const ERROR:int 	= 4;
		private static const FATAL:int 	= 5;
		
		private static var host:String = "127.0.0.1";
		private static var port:int = 8888;
		private static var timeout:int = 10;
		private static var socket:Socket = new Socket();
		private static var isFirstConnect:Boolean = true;
		private static var connected:Boolean;
		private static var isServerRun:Boolean = true;
		private static var logVec:Vector.<LogVO> = new Vector.<LogVO>();
		
		{
			socket = new Socket();
			//添加事件监听器，连接成功时，socket实例会广播Event.CONNECT事件
			socket.addEventListener(Event.CONNECT, onConnect);
			socket.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
			socket.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onSecurityError);
			socket.timeout = timeout;
			socket.connect(host, port);
			trace("开始连接: host=" + host + " port=" + port);
		}
		
		private static function onConnect(event:Event):void{
			connected = true;
			isServerRun = true;
			for each (var logVO:LogVO in logVec) {
				log(logVO.level, logVO.msg);	
			}
			logVec = null;
			trace("已连接到服务器");
		}
		
		private static function onIOError(event:IOErrorEvent):void {
			trace("-->" + event);
			trace("onIOError1 " + i++);
			dispose();
			trace("onIOError2 " + i++);
		}
		
		private static function onSecurityError(event:SecurityErrorEvent):void {
			trace("-->" + event);
			trace("onSecurityError1 " + i++);
			dispose();
			trace("onSecurityError2 " + i++);
		}
		
		private static var i:int = 0;
		
		private static function dispose():void {
			trace("dispose" + i++);
			logVec = null;
			connected = false;
			isServerRun = false;
			if(socket) {
				trace("close1 " + i++);//close2
				try {
					socket.close();	
				}catch(err:IOError) {
					
				}
				trace("close2 " + i++);
				if(socket) {
					socket.removeEventListener(Event.CONNECT, onConnect);
					socket.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
					socket = null;		
				}
			}
			trace("dispose end " + i++);
		}
		
		public function FlashLog() {
			throw new TypeError(getQualifiedClassName(FlashLog) + "不是构造函数。")
		}
		
		public static function debug(msg:String):void {
			trace("bbbb");
			log(FlashLog.DEBUG, msg);
		}
		
		public static function info(msg:String):void {
			log(FlashLog.INFO, msg);
		}
		
		public static function warn(msg:String):void {
			log(FlashLog.WARN, msg);
		}
		
		public static function error(msg:String):void {
			log(FlashLog.ERROR, msg);
		}
		
		public static function fatal(msg:String):void {
			log(FlashLog.FATAL, msg);
		}
		
		private static function log(level:int, msg:String):void {
			if(connected) {
				var len:int = msg.length + 5;
				socket.writeInt(len);
				socket.writeUTFBytes(level + msg);	
				socket.flush();//flush的时候向Server发数据	
			}else if(isServerRun) {
				var data:Object = {};
				data.level 	= level;
				data.msg 	= msg;
				var logVO:LogVO = new LogVO(data);
				logVec.push(logVO);	
			}
		}
	}
}



