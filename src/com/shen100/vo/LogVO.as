package com.shen100.vo
{
	public class LogVO
	{
		private static const DEBUG:int 	= 1;
		private static const INFO:int 	= 2;
		private static const WARN:int 	= 3;
		private static const ERROR:int 	= 4;
		private static const FATAL:int 	= 5;
		
		public var time:Number;
		public var level:int;
		public var msg:String;
		
		public function LogVO(data:Object)
		{
			time = data.time;
			level = data.level;
			msg = data.msg;
		}
	}
}