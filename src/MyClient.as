package
{
	import com.shen100.FlashLog;
	import com.shen100.vo.LogVO;
	
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	
	public class MyClient extends Sprite
	{
		private var msg:TextField;
		private var send:Sprite;
		
		private var logVec:Vector.<LogVO>;
		
		public function MyClient() {
		
			for each (var logVO:LogVO in logVec) {
				trace(1);	
			}
			
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage(event:Event):void {
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			stage.align = StageAlign.TOP_LEFT;
			stage.scaleMode = StageScaleMode.NO_SCALE;
			
			msg = new TextField();
			addChild(msg);
			msg.type = TextFieldType.INPUT;
			msg.border = true;
			msg.width = 120;
			msg.height = 26;
			
			send = new Sprite();
			send.graphics.beginFill(0x888888);
			send.graphics.drawRect(0, 0, 50, 26);
			send.graphics.endFill();
			addChild(send);
			send.x = msg.x + msg.width + 5;
			
			send.addEventListener(MouseEvent.CLICK, onClick);
			FlashLog.debug("aaaa");
		}
		
		private function onClick(event:MouseEvent):void
		{
			if(msg.text) {
				FlashLog.debug(msg.text);
			}
		}
	}
}














